﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    [Header("Music file")]
    public AudioClip m_audioClip;

    [Header("Audio Setting")]
    [Range(0f, 1f)] public float m_MaxVolume;
    [Range(0f, 1f)] public float m_MinVolume;
    [Range(0.01f, 1f)] public float m_FadeSpeed;
    public bool m_EnableFadeIn;
    public bool m_EnableFadeOut;
    public bool m_Repeat = true;

    public AudioSource audioSource;

    void Awake()
    {
        audioSource.volume = m_MinVolume;
        audioSource.loop = m_Repeat;
    }

    public void SetAudioClipAndPlay(AudioClip _clip)
    {
        audioSource.clip = _clip;
        Play();
    }

    void Play()
    {
        StartCoroutine(FadeIn());
    }

    public void Stop()
    {
        StartCoroutine(FadeOut());
    }


    IEnumerator FadeIn()
    {
        audioSource.Play();

        while (m_EnableFadeIn)
        {
            if (audioSource.volume > m_MaxVolume - 0.01) break;

            audioSource.volume = Mathf.Lerp(audioSource.volume, m_MaxVolume, m_FadeSpeed);
            yield return new WaitForFixedUpdate();
        }
        
    }

    IEnumerator FadeOut()
    {
        while (m_EnableFadeOut)
        {
            if (audioSource.volume < 0.1f) break;

            audioSource.volume = Mathf.Lerp(audioSource.volume, 0f, m_FadeSpeed);
            yield return new WaitForFixedUpdate();
        }

        audioSource.Stop();
    }
}
