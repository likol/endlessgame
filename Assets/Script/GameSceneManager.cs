﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : MonoBehaviour
{
    [Header("Settings")]
    public float m_SkyboxMovingSpeed = 2f;

    public float m_cameraFollowSpeed = 0.08f;

    Camera mainCamera;
    GameObject rotationTarget;

    public Vector3 offset;

    protected void Awake()
    {
        mainCamera = Camera.main;
        rotationTarget = GameObject.Find("RotationTarget");
    }

    private void Start()
    {
        mainCamera.transform.localPosition = offset;
    }

    // Update is called once per frame
    void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * this.m_SkyboxMovingSpeed);
    }

    void FixedUpdate()
    {
        Transform target = null;

        if (UnitStatusUIManager.Instance.m_currentHoverUnit)
            target = UnitStatusUIManager.Instance.m_currentHoverUnit.transform;
        else if (BattleManger.Instance.m_CurrentActionUnit)
            target = BattleManger.Instance.m_CurrentActionUnit.transform;

        if (target)
            rotationTarget.transform.position = Vector3.Slerp(rotationTarget.transform.position, target.transform.position, m_cameraFollowSpeed);
    }

    private void OnApplicationQuit()
    {
        RenderSettings.skybox.SetFloat("_Rotation", 0.0F);
    }
}
