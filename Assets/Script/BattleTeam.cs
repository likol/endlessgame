﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BattleTeam
{
    public List<GridObject> m_Units;
    protected string teamName;

    public BattleTeam(string teamTag)
    {
        this.teamName = teamTag;
        this.m_Units = new List<GridObject>();
    }

}
