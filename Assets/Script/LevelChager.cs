﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChager : MonoBehaviour
{
    public Animator animator;

    private int levelIndex;

    public static LevelChager Instance = null;

    public MusicPlayer m_MusicPlayer;

    public AudioClip m_AudioClip;

    protected void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            DestroyImmediate(gameObject);
        }
    }

    public void FadeToLevel(int levelIndex)
    {
        this.levelIndex = levelIndex;
        if (m_AudioClip)
        {
            m_MusicPlayer.Stop();
            StartCoroutine(WaitForMusicStop());
        }
        else
        {
            animator.SetTrigger("FadeOut");
        }
    }

    IEnumerator WaitForMusicStop()
    {
        yield return new WaitUntil(() => !m_MusicPlayer.audioSource.isPlaying);
        animator.SetTrigger("FadeOut");
    }

    public void OnLevelFadeInComplete()
    {
        if (m_AudioClip)
            m_MusicPlayer.SetAudioClipAndPlay(this.m_AudioClip);
    }
    public void OnLevelFadeOutComplete()
    {
        if (levelIndex == -1)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.ExitPlaymode();
#else
            Application.Quit();
#endif
            return;
        }
        SceneManager.LoadScene(levelIndex);
    }
}
;
