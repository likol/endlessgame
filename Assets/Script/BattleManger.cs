﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.Linq;

public class BattleManger : MonoBehaviour
{
    public delegate void ChangeCurrentActionUnitHandler(GridObject unit);
    public delegate void ActionUnitChangedHandler(GridObject unit);
    public event ChangeCurrentActionUnitHandler OnCurrentUnitChanged;
    public event ActionUnitChangedHandler OnQueueEnqueue;
    public event ActionUnitChangedHandler OnQueueDequeue;

    [Header("Selected Unit")]
    public GridObject m_CurrentActionUnit = null;

    [Header("Players")]
    public BattleTeam m_Player = new BattleTeam("Player");

    [Header("Enemys")]
    public BattleTeam m_Enemy = new BattleTeam("Enemy");

    public List<GridObject> m_AllBattleUnit = new List<GridObject>();

    public Queue<GridObject> m_UnitActionQueue = new Queue<GridObject>();

    public static BattleManger Instance = null;

    [Header("Gameover UI")]
    public GameObject m_gameWin;
    public GameObject m_gameLose;


    protected void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }


    // 建立玩家與敵方的List
    void Start()
    {
        GridObject[] teamUnits = FindObjectsOfType<GridObject>();

        foreach (GridObject unit in teamUnits)
        {
            switch (unit.tag)
            {
                case "Player":
                    this.m_Player.m_Units.Add(unit);
                    break;
                case "Enemy":
                    this.m_Enemy.m_Units.Add(unit);
                    break;
                default:
                    break;
            }
        }

        // 將所有可行動單位加入至戰鬥單位列表
        m_AllBattleUnit.AddRange(this.m_Player.m_Units);
        m_AllBattleUnit.AddRange(this.m_Enemy.m_Units);

    }

    void FixedUpdate()
    {
        if (this.m_CurrentActionUnit == null && this.m_UnitActionQueue.Count > 0)
        {
            SetActionUnit(this.m_UnitActionQueue.Dequeue());
            OnQueueDequeue(m_CurrentActionUnit);
        }

        // 當行動佇列長度小於所有戰鬥單位數量時，使用協程更新所有戰鬥單位行動累計值
        if (this.m_UnitActionQueue.Count < this.m_AllBattleUnit.Count)
            StartCoroutine(UpdateUnitActionStep());

        // 當佇列不為空時，更新佇列中所有單位的累計行動值
        // 直接在update 中做排序會造成卡頓
        // 未來在UI上要做目前行動隊列的顯示，故這邊先移除
        /*
        if (this.m_CurrentActionUnit == null)
        {
            this.UpdateUnitSpeed();
            this.SortActionQueue();
            // 當佇列中累計行動值最高的單位以達到 100 則設該單位為目前行動單位
            if (this.m_UnitActionQueue[0].GetComponent<MoveController>().m_Progressing >= 100)
                this.SetActionUnit();
        }
        */

    }

    IEnumerator UpdateUnitActionStep()
    {

        foreach (GridObject unit in this.m_AllBattleUnit)
        {
            bool isFinished = unit.GetComponent<Controller>().UpdateProgressing();

            if (isFinished && !m_UnitActionQueue.Contains(unit))
            {
                m_UnitActionQueue.Enqueue(unit);
                OnQueueEnqueue(unit);
            }
                
        }

        yield return null;
    }

    /// <summary>
    /// 排序行動佇列
    /// </summary>
    /*
    private void SortActionQueue()
    {
        this.m_UnitActionQueue.Sort(
            Comparer<GridObject>.Create((x, y) =>
            x.GetComponent<MoveController>().m_Progressing < y.GetComponent<MoveController>().m_Progressing ? 1 :
            x.GetComponent<MoveController>().m_Progressing == y.GetComponent<MoveController>().m_Progressing ? 0 :
            -1));
    }
    */

    /// <summary>
    /// 設定當前行動單位
    /// </summary>
    private void SetActionUnit(GridObject unit)
    {
        this.m_CurrentActionUnit = unit;
        this.m_CurrentActionUnit.GetComponent<Controller>().OnAction();
        this.OnCurrentUnitChanged(this.m_CurrentActionUnit);
    }

    /// <summary>
    /// 結束當前行動單位的回合
    /// </summary>
    public void ExitActionUnit()
    {
        this.m_CurrentActionUnit = null;
        this.OnCurrentUnitChanged(null);
    }

    public void RemoveUnit(GridObject unit)
    {
        string unitTag = unit.tag;
        //Remove from allUnit list
        m_AllBattleUnit.Remove(unit);
        //Remove from ActionQueue
        m_UnitActionQueue = new Queue<GridObject>(m_UnitActionQueue.Where(item => item != unit));

        //Remove from BattleTeam list
        switch (unitTag)
        {
            case "Player":
                m_Player.m_Units.Remove(unit);
                break;
            case "Enemy":
                m_Enemy.m_Units.Remove(unit);
                break;
        }

        DetectIsGameOver();
    }

    public void DetectIsGameOver()
    {
        if (m_Player.m_Units.Count == 0)
        {
            m_gameLose.SetActive(true);
        }
            

        if (m_Enemy.m_Units.Count == 0)
        {
            m_gameWin.SetActive(true);
        }
    }
}
