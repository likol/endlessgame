﻿using Equipment;
using JobSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weapon;

namespace JobSystem
{
    public class SwordsMan : JobAbstract
    {
        public SwordsMan()
        {
            JobName = "劍士";
            Agile += 2;
            ActivePoint += 2;
            AtkRange += 1;
            AtkPower += 10;
            MaxHealth = Health;
        }
        public override void AttachEquipment(EquipmentAbstract equipment)
        {
            return;
        }

        public override void AttachWeapon(WeaponAbstract weapon)
        {
            return;
        }

        public override IEnumerator doAttack(GridObject target)
        {
            var rotateDirection = (target.m_GridPosition - GetComponent<GridObject>().m_GridPosition);

            transform.localRotation = GetComponent<GridMovement>().DirectionToRotation(rotateDirection);
            GetComponent<GridObject>().m_FacingDirection = rotateDirection;
            Animator m_Animator = transform.GetComponentInChildren<Animator>();

            m_Animator.SetTrigger("Attacking");
            DestroyVirtualPath();
            //GetComponent<PlayerController>().GoToIdle();
            yield return new WaitUntil(() => m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attacking"));
            //yield return new WaitUntil(() => m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"));

            IEnumerator takeDamage = target.GetComponent<JobAbstract>().doTakeDamage(AtkPower, this.gameObject);
            StartCoroutine(takeDamage);
        }

        public override IEnumerator doTakeDamage(int AtkPower, GameObject Source)
        {
            this.Health -= (AtkPower - Defence);

            Debug.LogFormat("atk power: {0}, def: {1}, current health: {2}", AtkPower, Defence, Health);
            Animator m_Animator = transform.GetComponentInChildren<Animator>();
            if (Health > 0)
            {
                m_Animator.SetTrigger("Damaging");
                yield return new WaitUntil(() => m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Damaging"));
            } else
            {
                m_Animator.SetTrigger("Dead");
                yield return new WaitUntil(() => m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Dead"));
                yield return new WaitForSeconds(1.5F);
                doDead();
                Source.GetComponent<Controller>().EndTurn();
                yield break;
            }
            
            
            yield return new WaitUntil(() => m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle"));
            Source.GetComponent<Controller>().EndTurn();
        }
    }

}
