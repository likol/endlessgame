﻿using Equipment;
using JobSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Weapon;

namespace JobSystem
{
    public class Monk : JobAbstract
    {
        public override void AttachEquipment(EquipmentAbstract equipment)
        {
            return;
        }

        public override void AttachWeapon(WeaponAbstract weapon)
        {
            return;
        }

        public override IEnumerator doAttack(GridObject target)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerator doTakeDamage(int AtkPower, GameObject Source)
        {
            throw new System.NotImplementedException();
        }
    }

}
