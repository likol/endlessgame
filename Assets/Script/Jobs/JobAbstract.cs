﻿using Equipment;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Weapon;

namespace JobSystem
{
    public abstract class JobAbstract : MonoBehaviour
    {
        public delegate void OnDeadHandler(GridObject unit);
        public event OnDeadHandler OnDeadEvent;
        public virtual string JobName { get => m_jobName; set => m_jobName = value; }
        public virtual int ActivePoint { get => m_activePoint; set => m_activePoint = value; }
        public virtual int Health { get => m_health; set => m_health = value; }
        public virtual int MaxHealth { get => m_maxHealth; set => m_maxHealth = value; }
        public virtual int CurrentHealthPercent { get => (int)((float)Health / MaxHealth * 100); }
        public virtual int MagicPont { get => m_magicPont; set => m_magicPont = value; }
        public virtual int AtkPower { get => m_atkPower; set => m_atkPower = value; }
        public virtual int Defence { get => m_defence; set => m_defence = value; }
        public virtual int Agile { get => m_agile; set => m_agile = value; }
        public virtual int AtkRange { get => m_atkRange; set => m_atkRange = value; }
        public virtual int AtkOffset { get => m_atkOffset; set => m_atkOffset = value; }
        public virtual AttackType AtkType { get => m_atkType; set => m_atkType = value; }
        public virtual List<GridTile> CalculatedAtkRange { get => m_calculatedAtkRange; set => m_calculatedAtkRange = value; }
        public virtual string VirtualHolder { get => m_virtualHolder; set => m_virtualHolder = value; }
        public GameObject NodePrefab { get => m_NodePrefab; set => m_NodePrefab = value; }
        public string AttackAnimationTrigger { get => m_AttackAnimationTrigger; set => m_AttackAnimationTrigger = value; }
        public string DamageAnimationTrigger { get => m_DamageAnimationTrigger; set => m_DamageAnimationTrigger = value; }
        public string DeadAnimationTrigger { get => m_DeadAnimationTrigger; set => m_DeadAnimationTrigger = value; }

        [Header("Status")]
        [SerializeField] private string m_jobName;
        [SerializeField] private int m_activePoint = 5;
        [SerializeField] private int m_health = 100;
        [SerializeField] private int m_maxHealth = 100;
        [SerializeField] private int m_magicPont = 100;
        [SerializeField] private int m_atkPower = 20;
        [SerializeField] private int m_defence = 10;
        [SerializeField] private int m_agile = 8;
        [SerializeField] private int m_atkRange = 1;
        [SerializeField] private int m_atkOffset = 0;
        [SerializeField] private string m_virtualHolder = "攻擊範圍";
        [SerializeField] public Sprite m_avatarImage;

        private List<GridTile> m_calculatedAtkRange = new List<GridTile>();

        [SerializeField] private AttackType m_atkType = AttackType.LINEAR;

        private GameObject m_NodePrefab;

        private string m_AttackAnimationTrigger;

        private string m_DamageAnimationTrigger;

        private string m_DeadAnimationTrigger;

        public enum AttackType
        {
            CIRCLE,
            LINEAR,
            SHOT,
            PROJECTILE
        }
        public virtual void Awake()
        {
            m_NodePrefab = Resources.Load<GameObject>("RedVisualizer");
        }

        public abstract void AttachWeapon(WeaponAbstract weapon);

        public abstract void AttachEquipment(EquipmentAbstract equipment);

        public virtual void StartAttack()
        {
            Debug.LogFormat("Unit: {0} Job: {1} ----- Start draw AtkRange Type: {2} -----", gameObject.name, JobName, AtkType);

            switch (AtkType)
            {
                case AttackType.LINEAR:
                    drawAttackLinearRange(gameObject.GetComponent<GridObject>());
                    break;
                default:
                    return;
            }

            DisplayVirtualPath();
        }
        public virtual void Attack(GridObject target)
        {
            if (gameObject.tag == target.tag) return;
            bool isInRange = CalculatedAtkRange.Contains(target.m_CurrentGridTile);
            if (isInRange != true) return;
            StartCoroutine(doAttack(target));
        }

        public abstract IEnumerator doAttack(GridObject target);

        public abstract IEnumerator doTakeDamage(int AtkPower, GameObject Source);

        public virtual void doDead()
        {
            OnDeadEvent(gameObject.GetComponent<GridObject>());
            BattleManger.Instance.RemoveUnit(GetComponent<GridObject>());
            GetComponent<Controller>().m_UnitState = Controller.UnitState.Dead;
            BattleManger.Instance.ExitActionUnit();
        }

        public virtual void drawAttackCircleRange(GridObject Player)
        {
            CalculatedAtkRange.Clear();
            CalculatedAtkRange = GridManager.GetAttackableGrids(Player.m_CurrentGridTile, AtkRange);
        }

        public virtual void drawAttackLinearRange(GridObject Player)
        {
            CalculatedAtkRange.Clear();

            CalculatedAtkRange = RangeAlgorithms.LinearRange(Player.m_CurrentGridTile, AtkRange);
        }

        public virtual void DisplayVirtualPath()
        {
            VirtualPathManager.Instance.CreateVisualizer(VirtualHolder, CalculatedAtkRange, NodePrefab);
        }

        public virtual void DestroyVirtualPath()
        {
            VirtualPathManager.Instance.Remove(VirtualHolder);
        }

        /*public virtual void OnDead()
        {
            OnDeadEvent(gameObject.GetComponent<GridObject>());
        }*/
    }
}
