﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Cinemachine;

public class Scene1_P1 : MonoBehaviour
{
    private GridMovement _gridMovement;
    protected GridPathfinder _gridPathfinder;
    protected GridObject _gridObject;
    
    public AudioSource _audioSource;
    public AudioClip earthquake;
    public CinemachineVirtualCamera CM;

    public 
    void Awake()
    {
        _gridObject = GetComponent<GridObject>();
        _gridMovement = GetComponent<GridMovement>();
        _gridPathfinder = GetComponent<GridPathfinder>();
    }

    public void GoPos1()
    {
        _gridPathfinder.SetNewDestination(GridManager.GetGridTileAtPosition(new Vector2Int(-57, 3)));
    }

    public void GoPos2()
    {
        _gridPathfinder.SetNewDestination(GridManager.GetGridTileAtPosition(new Vector2Int(-50, 3)));
    }

    public void GoPos3()
    {
        _gridPathfinder.SetNewDestination(GridManager.GetGridTileAtPosition(new Vector2Int(-54, -1)));
    }

    public void PlayIdle2()
    {
        transform.GetChild(0).GetComponent<Animator>().SetTrigger("Idle2");
    }

    public void PlayIdle3()
    {
        transform.GetChild(0).GetComponent<Animator>().SetTrigger("Idle3");
    }
    public void PlayIdle4()
    {
        transform.GetChild(0).GetComponent<Animator>().SetTrigger("Idle4");
    }

    public void P3Enter_P1()
    {
        _gridMovement.AnimateRotateTo(new Vector2Int(1, -1));
    }

    public void P3Enter_P2()
    {
        _gridMovement.AnimateRotateTo(new Vector2Int(1, 0));
    }

    public void P3Enter_P3()
    {
        transform.GetComponentInChildren<Renderer>().enabled = true;
        CM.LookAt = transform;
    }

    public void StartShake()
    {
        CinemachineBasicMultiChannelPerlin perlin = CM.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        perlin.m_AmplitudeGain = 1;

        _audioSource.clip = earthquake;

        _audioSource.Play();
    }

    public void CameraShake(float intensity)
    {
        CinemachineBasicMultiChannelPerlin perlin = CM.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        perlin.m_AmplitudeGain = intensity;
    }


    public void PlayP3Motion()
    {
        transform.GetChild(0).GetComponent<Animator>().SetTrigger("Motion");
    }

    public void Final()
    {
        Debug.Log("Final");
        LevelChager.Instance.FadeToLevel(3);
    }

    public void OpenDoorSound()
    {
        _audioSource.Play();
    }
}
