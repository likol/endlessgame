﻿using JobSystem;
using UnityEngine;
public class UnitUIManager : MonoBehaviour
{
    public static UnitUIManager Instance = null;

    [Header("Selected Unit")]
    public GridObject m_CurrentActionUnit = null;

    private GameObject m_UnitActionMenuRoot = null;

    [Header("Offset")]
    public float m_UIOffsetX = 0F;
    public float m_UIOffsetY = 0F;

    public AudioSource audioSource;

    public AudioClip OpenSound;
    public AudioClip ClickSound;



    private float UIScale;

    public enum State {
        Close,
        Open
    }

    protected void Awake()
    {
        m_UnitActionMenuRoot = transform.GetChild(0).gameObject;

        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void SetVisible(bool _bool)
    {
        m_UnitActionMenuRoot.SetActive(_bool);
    }

    public void UnitActionMenu(GridObject gridObject)
    {
        if (m_UnitActionMenuRoot.activeSelf) return;
        
        PlayOpenSound();
        m_CurrentActionUnit = gridObject;
        SetVisible(true);
        m_UnitActionMenuRoot.GetComponent<RectTransform>().anchoredPosition = new Vector3(m_UIOffsetX, m_UIOffsetY, 0);
    }

    public void StandByHandler()
    {
        PlayClickSound();
        SetVisible(false);
        m_CurrentActionUnit.GetComponent<Controller>().EndTurn();
    }

    public void ReturnHandler()
    {
        PlayClickSound();
        m_CurrentActionUnit.GetComponent<PlayerController>().GoBackToOriginalPos();
        SetVisible(false);
    }

    public void AttackHandler()
    {
        PlayClickSound();
        m_CurrentActionUnit.GetComponent<PlayerController>().StartAttack();
        m_CurrentActionUnit.GetComponent<JobAbstract>().StartAttack();
        SetVisible(false);
    }

    public void PlayClickSound()
    {
        audioSource.clip = ClickSound;
        audioSource.Play();
    }

    public void PlayOpenSound()
    {
        audioSource.clip = OpenSound;
        audioSource.Play();
    }
}
