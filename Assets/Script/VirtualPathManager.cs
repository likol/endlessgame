﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class VirtualPathManager : MonoBehaviour
{
    private Dictionary<string, Transform> VisualPathHolder = new Dictionary<string, Transform>();

    private string m_virtualPathName = "VirtualRoot";

    private Transform rootTransform;

    public static VirtualPathManager Instance = null;

    void Awake()
    {
        if (Instance == null) {
            Instance = this;
        } else if (Instance != this) {
            Debug.Log("重複的 VirtualPathManager Instance.");
            DestroyImmediate(this);
            return;
        }

        rootTransform = new GameObject(m_virtualPathName).transform;
    }

    public Transform Add(string _key)
    {
        Transform newVirtual = new GameObject(_key).transform;
        newVirtual.parent = rootTransform;
        VisualPathHolder.Add(_key, newVirtual);

        return newVirtual;
    }

    public Transform Get(string _key)
    {
        return VisualPathHolder[_key].transform;
    }

    public bool KeyExists(string _key)
    {
        return VisualPathHolder.ContainsKey(_key);
    }

    public bool Remove(string _key)
    {
        if (KeyExists(_key))
            DestroyImmediate(VisualPathHolder[_key].gameObject);

        return VisualPathHolder.Remove(_key);
    }

    public bool CreateVisualizer(string _key, List<GridTile> tiles, GameObject _Prefab)
    {
        if (KeyExists(_key)) return false;

        Transform newRoot = Add(_key);

        foreach(GridTile tile in tiles)
        {
            var newNode = Instantiate(_Prefab, tile.m_WorldPosition, Quaternion.identity).transform; ;
            newNode.parent = newRoot;
        }

        return true;
    }
}
