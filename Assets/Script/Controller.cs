﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : MonoBehaviour
{
    [ReadOnly] protected int m_Speed;
    [ReadOnly] protected int m_Progressing = 0;
    protected List<GridTile> _currentRange = new List<GridTile>();
    public UnitState m_UnitState;
    protected string _holderName = "移動範圍指示";
    public abstract void EndTurn();
    public enum UnitState
    {
        Standby,
        Idle,
        Moving,
        Attacking,
        UnitAction,
        Dead,
        SearchTarget,
        UpdateTarget,
        ChaseTarget,
        Escape,
        Busy,
    }

    public virtual bool UpdateProgressing()
    {
        this.m_Progressing += this.m_Speed;

        if (this.m_Progressing >= 100)
        {
            this.m_Progressing -= 100;
            return true;
        }

        return false;
    }

    protected virtual void ClearVisualPath()
    {
        VirtualPathManager.Instance.Remove(_holderName);
        _currentRange.Clear();
    }

    public abstract void OnMoving();

    public abstract void OnAction();
}
