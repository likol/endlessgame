﻿using JobSystem;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(GridPathfinder))]
[RequireComponent(typeof(JobAbstract))]
public class PlayerController : Controller
{
    protected GridPathfinder _gridPathfinder;
    protected GridObject _gridObject;
    protected GridMovement _gridMovement;
    protected GameObject _highLightPoint;

    [Header("Settings")]
    [ReadOnly] private int m_ActivePoint;
    public GameObject m_NodePrefab;
    public Vector3 m_NodeWorldPositionOffSet;
    [ReadOnly] public JobAbstract m_job;

    private GridTile previousPos = null;
    private Quaternion previousRotate;
    private Vector2Int previoisFacing;


    // On awake we get the GridPathfinder component
    protected virtual void Awake()
    {
        m_UnitState = UnitState.Standby;
        _holderName = "玩家行動範圍指示";
        _gridObject = GetComponent<GridObject>();
        _gridMovement = GetComponent<GridMovement>();
        _gridPathfinder = this.gameObject.GetComponent<GridPathfinder>();
        _highLightPoint = GameObject.Find("MousePointTile");
        m_job = gameObject.GetComponent<JobAbstract>();

        m_Speed = m_job.Agile;
        m_ActivePoint = m_job.ActivePoint;
        //Register Listenter
        _gridMovement.OnMovementEnd += this.OnPathTail;
    }

    private void Start()
    {

    }

    // On Update we look for a mouse click
    protected virtual void Update()
    {
        if (BattleManger.Instance.m_CurrentActionUnit != _gridObject) return;

        switch (m_UnitState)
        {
            case UnitState.Standby:
                break;
            case UnitState.Idle:
                OnMoving();
                break;
            case UnitState.UnitAction:
                UnitUIManager.Instance.UnitActionMenu(_gridObject);
                break;
            case UnitState.Moving:
                if (EventSystem.current.IsPointerOverGameObject()) return;

                if (!IsHoverTillInRange(_currentRange))
                {
                    _highLightPoint.transform.GetComponentInChildren<Renderer>().material.SetColor("_Color", Color.gray);
                    return;
                }

                _highLightPoint.transform.GetComponentInChildren<Renderer>().material.SetColor("_Color", Color.red);

                if (Input.GetMouseButtonDown(0) && GridManager.Instance.m_HoveredGridTile != null)
                {
                    _gridPathfinder.SetNewDestination(GridManager.Instance.m_HoveredGridTile);
                }

                if (Input.GetMouseButtonDown(1))
                {
                    GoActionState();
                }
                return;

            case UnitState.Attacking:
                if (!IsHoverTillInRange(GetComponent<JobAbstract>().CalculatedAtkRange))
                {
                    _highLightPoint.transform.GetComponentInChildren<Renderer>().material.SetColor("_Color", Color.gray);
                    return;
                }

                _highLightPoint.transform.GetComponentInChildren<Renderer>().material.SetColor("_Color", Color.red);

                if (Input.GetMouseButtonDown(0))
                {
                    if (GridManager.Instance.m_HoveredGridObject != null)
                    {
                        if (GridManager.Instance.m_HoveredGridObject.GetComponent<Controller>().m_UnitState == UnitState.Dead) return;
                        GetComponent<JobAbstract>().Attack(GridManager.Instance.m_HoveredGridObject);
                    }
                }
                if (Input.GetMouseButtonDown(1))
                {
                    GetComponent<JobAbstract>().DestroyVirtualPath();
                    m_UnitState = UnitState.UnitAction;
                }

                break;
            default:
                break;
        }

    }

    private bool IsHoverTillInRange(List<GridTile> tiles)
    {
        var targetTile = GridManager.Instance.m_HoveredGridTile;

        return (targetTile == null || tiles.Contains(targetTile));
    }

    public void drawVisualizerMoveRange()
    {
        if (_currentRange.Count > 0) return;

        _currentRange = RangeAlgorithms.WalkableRangeGrid(_gridObject.m_CurrentGridTile, m_ActivePoint);
        VirtualPathManager.Instance.CreateVisualizer(_holderName, _currentRange, m_NodePrefab);
    }

    public override void OnMoving()
    {
        m_Progressing -= 100;
        previousPos = _gridObject.m_CurrentGridTile;
        previousRotate = _gridObject.transform.rotation;
        previoisFacing = _gridObject.m_FacingDirection;
        drawVisualizerMoveRange();
        m_UnitState = UnitState.Moving;
    }

    public void OnPathTail(GridMovement gridMovement, GridTile startGridTile, GridTile endGridTile)
    {
        if (_gridPathfinder.m_NextPathpointIndex == -1 && endGridTile.m_OccupyingGridObjects.First() == _gridObject)
            this.GoActionState();
    }

    public void GoActionState()
    {
        ClearVisualPath();
        m_UnitState = UnitState.UnitAction;
    }

    public void GoBackToOriginalPos()
    {
        _gridMovement.MoveTo(previousPos, false, false);
        transform.rotation = previousRotate;
        _gridObject.m_FacingDirection = previoisFacing;
        m_UnitState = UnitState.Moving;
        drawVisualizerMoveRange();
    }

    public void StartAttack()
    {
        m_UnitState = UnitState.Attacking;

    }

    public override void EndTurn()
    {
        m_UnitState = UnitState.Standby;
        BattleManger.Instance.ExitActionUnit();
    }

    public override void OnAction()
    {
        m_UnitState = UnitState.Idle;
    }
}
