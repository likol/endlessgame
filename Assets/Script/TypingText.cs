﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypingText : MonoBehaviour
{
    Text text;
    string story;
    public float typingSpeed = .125f;

    void Awake()
    {
        text = GetComponent<Text>();
        story = text.text;
        text.text = "";
        StartCoroutine("Typing");
    }

    IEnumerator Typing()
    {
        yield return new WaitForSeconds(2F);

        foreach (char c in story)
        {
            text.text += c;
            yield return new WaitForSeconds(typingSpeed);
        }

        yield return new WaitForSeconds(4F);
        LevelChager.Instance.FadeToLevel(2);
    }
}
