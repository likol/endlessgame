﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotKeys : MonoBehaviour
{
    [Header("Camera Settings:")]
    public float m_maxFieldOfView = 8;
    public float m_minFieldOfView = 3;
    [Range(0.01f, 0.1f)]
    

    public static HotKeys Instance = null;
    public float smoothRotateSpeed = 2f;

    Camera mainCamera;
    GameObject rotationTarget;


    protected void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        mainCamera = Camera.main;
        rotationTarget = GameObject.Find("RotationTarget");
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        

    }

    void LateUpdate()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            LevelChager.Instance.FadeToLevel(0);
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (mainCamera.orthographicSize < m_maxFieldOfView)
                mainCamera.orthographicSize++;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            if (mainCamera.orthographicSize > m_minFieldOfView)
                mainCamera.orthographicSize--;
        }

        if (Input.GetMouseButton(2))
        {
            rotationTarget.transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X") * smoothRotateSpeed, 0));
        }
    }

}
