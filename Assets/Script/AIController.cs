﻿using JobSystem;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(GridPathfinder))]
[RequireComponent(typeof(JobAbstract))]
public class AIController : Controller
{
    protected GridPathfinder _gridPathfinder;
    protected GridObject _gridObject;
    protected GridMovement _gridMovement;


    [Header("Settings")]
    public bool m_searchOutterUnit = true;
    [ReadOnly] private int m_ActivePoint;
    [ReadOnly] public JobAbstract m_job;
    [ReadOnly] public GridObject m_CurrectTarget = null;
    [ReadOnly] public GridTile m_moveTargetTile = null;
    public GameObject m_NodePrefab;
    private List<GridTile> _attackableGrids = null;

    protected virtual void Awake()
    {
        m_UnitState = UnitState.Standby;
        _holderName = "敵方行動範圍指示";
        _gridObject = GetComponent<GridObject>();
        _gridMovement = GetComponent<GridMovement>();
        _gridPathfinder = this.gameObject.GetComponent<GridPathfinder>();
        m_job = gameObject.GetComponent<JobAbstract>();

        m_Speed = m_job.Agile;
        m_ActivePoint = m_job.ActivePoint;
    }

    void LateUpdate()
    {
        if (BattleManger.Instance.m_CurrentActionUnit != _gridObject) return;

        switch (m_UnitState)
        {
            case UnitState.Idle:
                _attackableGrids = GridManager.GetAttackableGrids(_gridObject.m_CurrentGridTile, m_ActivePoint);
                _currentRange = RangeAlgorithms.WalkableRangeGrid(_gridObject.m_CurrentGridTile, m_ActivePoint);

                m_UnitState = UnitState.SearchTarget;

                break;
            case UnitState.SearchTarget:
                Debug.Log("SearchTarget");
                SearchingTarget();
                m_UnitState = UnitState.ChaseTarget;
                break;
            case UnitState.ChaseTarget:
                m_UnitState = UnitState.Busy;
                Debug.Log("ChaseTarget");
                StartCoroutine(OnChase());
                break;
            case UnitState.Moving:
                m_UnitState = UnitState.Busy;
                Debug.Log("Moving");
                StartCoroutine(Move());
                break;
            case UnitState.Escape:
                m_UnitState = UnitState.Busy;
                Debug.Log("Escape");
                StartCoroutine(Escape());
                break;
            case UnitState.Attacking:
                m_UnitState = UnitState.Busy;
                Debug.Log("Attacking");
                StartCoroutine(Attacking());
                break;
            case UnitState.Standby:
                break;
            default:
                break;
        }
    }

    public IEnumerator OnChase()
    {
        yield return new WaitForSeconds(.5F);

        if (m_CurrectTarget.GetComponent<Controller>().m_UnitState == Controller.UnitState.Dead)
        {
            Debug.Log("Target Dead");
            m_UnitState = UnitState.SearchTarget;
            yield break;
        }
            

        if (m_job.CurrentHealthPercent < 25)
        {
            m_UnitState = UnitState.Escape;
            yield break;
        }

        if (CheckCurrectTileInAttackRange())
        {
            m_UnitState = UnitState.Attacking;
            yield break;
        } else
        {
            //取得離自己最近且可攻擊到目標的Tile
            var AttackabkeTiles = AttackableTileList(m_CurrectTarget).FindAll(tile => !tile.IsTileOccupied());
            //如果不存在可以攻擊的 Tile 則結束回合
            if (AttackabkeTiles.Count < 1)
            {
                EndTurn();
                yield break;
            }

            m_moveTargetTile = AttackabkeTiles.OrderBy(item => AStar.PathCost(_gridObject.m_CurrentGridTile, item)).ToList().First();

            m_UnitState = UnitState.Moving;
        }
    }

    public override void OnMoving()
    {
        StartCoroutine(Move());
    }

    public IEnumerator Escape()
    {
        // 隨機取得可移動範圍內的一個 Tile
        m_moveTargetTile = _currentRange[Random.Range(0, _currentRange.Count()-1)];
        MoveToTile();
        yield return new WaitUntil(() => _gridPathfinder.m_NextPathpointIndex == -1);
        // 回血
        m_job.Health += 15;
        EndTurn();
    }

    public IEnumerator Move()
    {
        if (MoveToTile())
        {
            yield return new WaitUntil(() => _gridPathfinder.m_NextPathpointIndex == -1);
            yield return new WaitForSeconds(.5F);
            m_UnitState = UnitState.Attacking;
        }
        else
        {
            yield return new WaitUntil(() => _gridPathfinder.m_NextPathpointIndex == -1);
            yield return new WaitForSeconds(.5F);
            EndTurn();
        }

    }

    public IEnumerator Attacking()
    {
        yield return new WaitForSeconds(.2F);

        GetComponent<JobAbstract>().StartAttack();
        StartCoroutine(m_job.doAttack(m_CurrectTarget));
    }

    public void drawVisualizerMoveRange()
    {      
        VirtualPathManager.Instance.CreateVisualizer(_holderName, _currentRange, m_NodePrefab);
    }

    public void SearchingTarget()
    {
        Debug.LogFormat("Unit: {0} start searching target.", gameObject.name);
        var targets = SearchTargetInWalkableRange();

        if (targets.Count() == 1)
        {
            m_CurrectTarget = targets[0];
            return;
        }

        var target = GetBestTargetChoice(targets);

        if (target)
            m_CurrectTarget = target;
        else
        {
            if (!m_searchOutterUnit)
            {
                EndTurn();
                return;
            }

            m_CurrectTarget = SearchAllAndGetMinCost();
        }
    }

    public bool MoveToTile()
    {
        drawVisualizerMoveRange();

        // 首先檢查是否可以移動，如果處在無法移動的狀態下先結束回合
        if (_currentRange.Count <= 1)
        {
            EndTurn();
            return false;
        }

        var path = AStar.Search(_gridObject.m_CurrentGridTile, m_moveTargetTile);
        if (path == null) return false;

        int tempActionPoint = m_job.ActivePoint;
        GridTile finalTile = null;

        foreach(GridTile tile in path)
        {
            if (_currentRange.Contains(tile))
            {
                tempActionPoint -= tile.Cost();
                if (tempActionPoint < 0)
                    break;

                finalTile = tile;
            }
        }

        if (finalTile)
        {
            _gridPathfinder.SetNewDestination(finalTile);

            return (finalTile == m_moveTargetTile);
                
        }
        
        return false;
    }

    public GridObject GetBestTargetChoice(List<GridObject> units)
    {
        GridObject targetPlayer = null;
        JobAbstract self = GetComponent<JobAbstract>();
        
        Dictionary<GridObject, int> selected = new Dictionary<GridObject, int>();

        foreach(GridObject unit in units)
        {
            JobAbstract playerStatus = unit.GetComponent<JobAbstract>();
            if (unit.tag == gameObject.tag) continue;

            int score = self.AtkPower - playerStatus.Defence;

            if (playerStatus.AtkPower >= self.AtkPower) score += 10;
            if (playerStatus.Defence >= self.Defence) score += 10;
            if (playerStatus.CurrentHealthPercent <= 50) score += 20;

            selected.Add(unit, score);
        }
        if (selected.Count > 0)
            targetPlayer = selected.Aggregate( (a, b) => a.Value > b.Value ? a : b).Key;

        return targetPlayer;
    }

    /// <summary>
    /// 搜尋並返回行動範圍內的所有玩家單位
    /// </summary>
    /// <returns></returns>
    public List<GridObject> SearchTargetInWalkableRange()
    {
        List<GridObject> results = new List<GridObject>();

        foreach(GridTile tile in _attackableGrids)
        {
            if (tile.m_OccupyingGridObjects == null || tile.m_OccupyingGridObjects.Count <= 0) continue;
            var target = tile.m_OccupyingGridObjects.First();
            if (!BattleManger.Instance.m_Player.m_Units.Contains(target)) continue;

            results.Add(target);
        }

        return results;
    }

    /// <summary>
    /// 搜尋所有戰場上的玩家單位，並返回最短路徑的玩家
    /// </summary>
    public GridObject SearchAllAndGetMinCost()
    {
        var allUnit = BattleManger.Instance.m_Player.m_Units;

        if (allUnit.Count < 1) return null;

        int tCost = 999;
        GridObject minCostUnit = null;

        allUnit.ForEach(unit =>
        {
            int cost = AStar.PathCost(_gridObject.m_CurrentGridTile, unit.m_CurrentGridTile);
            if (cost < tCost)
            {
                tCost = cost;
                minCostUnit = unit;
            }
        });

        return minCostUnit;
    }

    /// <summary>
    /// 搜尋所有戰場上的玩家單位，並返回最短的路徑
    /// </summary>
    public List<GridTile> SearchTargetInAllPlayer()
    {
        var allUnit = BattleManger.Instance.m_Player.m_Units;

        Dictionary<int, List<GridTile>> targetAndCost = new Dictionary<int, List<GridTile>>();

        allUnit.ForEach(unit =>
        {
            var path = AStar.Search(_gridObject.m_CurrentGridTile, unit.m_CurrentGridTile);
            if (path != null)
            {
                int cost = 0;
                List<GridTile> intersect = _currentRange.Intersect(path).ToList();

                intersect.ForEach(tile =>
                {
                    cost += tile.Cost();
                });

                if (!targetAndCost.ContainsKey(cost))
                    targetAndCost.Add(cost, intersect);
            }
        });

        if (targetAndCost.Count > 0)
            return targetAndCost.Aggregate((a, b) => a.Key < b.Key ? a : b).Value;
        else
            return null;
    }

    public override void EndTurn()
    {
        ClearVisualPath();
        m_UnitState = UnitState.Standby;
        BattleManger.Instance.ExitActionUnit();
    }

    public override void OnAction()
    {
        m_Progressing -= 100;
        this.m_UnitState = UnitState.Idle;
    }

    public List<GridTile> AttackableTileList(GridObject target)
    {
        return RangeAlgorithms.LinearRange(target.m_CurrentGridTile, m_job.AtkRange);
    }

    public bool CheckCurrectTileInAttackRange()
    {
        var ableAttackTiles = AttackableTileList(m_CurrectTarget);

        foreach(var tile in ableAttackTiles)
        {
            if (_gridObject.m_CurrentGridTile == tile) return true;
        }

        return false;
    }
}
