﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitStatusUIManager : MonoBehaviour
{
    public UnitStatus hover_status;
    public UnitStatus current_status;
    public GridObject m_currentHoverUnit;
    public static UnitStatusUIManager Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        //監聽當前指標單位改變的事件
        GridManager.Instance.OnHoverUnitChanged += HandlerHoverUnitChanged;

        //監聽當前行動單位改變的事件
        BattleManger.Instance.OnCurrentUnitChanged += HandlerCurrentUnitChanged;
    }


    public void HandlerHoverUnitChanged(GridObject unit)
    {
        hover_status.HandlerUnitChanged(unit);
    }

    void HandlerCurrentUnitChanged(GridObject unit)
    {
        current_status.HandlerUnitChanged(unit);
    }
}
