﻿using JobSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveOrderUI : MonoBehaviour
{
    public GameObject unitIcon;

    public Dictionary<GridObject, GameObject> m_unitIcon = new Dictionary<GridObject, GameObject>();

    public Transform iconParent;

    public Color playerColor;
    public Color enemyColor;

    // Start is called before the first frame update
    void Start()
    {
        BattleManger.Instance.OnQueueEnqueue += AddUnitIcon;
        BattleManger.Instance.OnQueueDequeue += DeleteUnitIcon;
    }

    void AddUnitIcon(GridObject unit)
    {
        if (!m_unitIcon.ContainsKey(unit))
        {
            unit.GetComponent<JobAbstract>().OnDeadEvent += DeleteUnitIcon;
            GameObject icon = Instantiate(unitIcon, iconParent);
            switch (unit.tag)
            {
                case "Player":
                    icon.GetComponent<Image>().color = playerColor;
                    break;
                case "Enemy":
                    icon.GetComponent<Image>().color = enemyColor;
                    break;
            }
            icon.transform.GetChild(0).GetComponent<Image>().sprite = unit.GetComponent<JobAbstract>().m_avatarImage;
            icon.GetComponent<UnitIcon>().unit = unit;

            m_unitIcon.Add(unit, icon);
        }
        
    }

    void DeleteUnitIcon(GridObject unit)
    {
        if (m_unitIcon.ContainsKey(unit))
        {
            m_unitIcon[unit].GetComponent<UnitIcon>().animator.SetBool("Display", false);
            m_unitIcon.Remove(unit);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(BattleManger.Instance.m_UnitActionQueue.Count);
    }
}
