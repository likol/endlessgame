﻿using JobSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitStatus : MonoBehaviour
{
    public Slider slider;
    public Gradient gradient;
    public Image fill;
    public Animator animator;
    public Text m_name;
    public Image m_avatar;

    public Image background;
    public Sprite playerBackground;
    public Sprite enemyBackground;

    private JobAbstract currentStatus;


    /// <summary>
    /// 監聽當前行動角色改變事件並控制狀態 UI 顯示
    /// </summary>
    /// <param name="unit"></param>
    public void HandlerUnitChanged(GridObject unit)
    {

        if (unit == null)
        {
            currentStatus = null;
            animator.SetBool("Display", false);
        }
        else
        {
            currentStatus = unit.GetComponent<JobAbstract>();

            SetMaxHealth(currentStatus.MaxHealth);
            SetHealth(currentStatus.Health);
            SetName(currentStatus.JobName);
            SetAvatar(currentStatus.m_avatarImage);
            SetBackground(currentStatus.tag);

            animator.SetBool("Display", true);
            //Debug.LogFormat("CurrentStatusUI Handler: {0}", unit.GetComponent<JobAbstract>().name);
        }
    }

    void SetAvatar(Sprite picture)
    {
        m_avatar.sprite = picture;
    }
    void SetName(string _name)
    {
        m_name.text = _name;
    }
    void SetMaxHealth(int health)
    {
        slider.maxValue = health;
        fill.color = gradient.Evaluate(1f);
    }

    void SetHealth(int health)
    {
        slider.value = health;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    void SetBackground(string teamTag)
    {
        switch (teamTag)
        {
            case "Player":
                background.sprite = playerBackground;
                break;
            case "Enemy":
                background.sprite = enemyBackground;
                break;
        }
    }

}
