﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitIcon : MonoBehaviour
{
    public GridObject unit;
    private UnitStatusUIManager StatusManager;
    public Animator animator;

    private void Start()
    {
        StatusManager = GameObject.Find("UnitStatusUI").GetComponent<UnitStatusUIManager>();
    }

    private void OnMouseEnter()
    {
        StatusManager.HandlerHoverUnitChanged(unit);
    }

    private void OnMouseExit()
    {
        StatusManager.HandlerHoverUnitChanged(null);
    }
    private void OnMouseDown()
    {
        UnitStatusUIManager.Instance.m_currentHoverUnit = unit;
    }

    void OnMouseUp()
    {
        UnitStatusUIManager.Instance.m_currentHoverUnit = null;
    }

    public void OnFadeOutEnd()
    {
        Destroy(gameObject);
    }
}
