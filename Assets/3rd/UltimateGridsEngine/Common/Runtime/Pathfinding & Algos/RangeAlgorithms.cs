﻿using System.Collections.Generic;
using Priority_Queue;
using UnityEngine;

public static class RangeAlgorithms {

    /// <summary>
    /// 原本的範圍演算法 SearchByGridPosition 在 Range >= 6 時幾乎無法計算，效能太差故重寫
    /// </summary>
    /// <param name="current"></param>
    /// <param name="actionPoint"></param>
    /// <returns></returns>
    public static List<GridTile> WalkableRangeGrid(GridTile current, int actionPoint)
    {
        System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        sw.Start();
        List<GridTile> tilesInRange = GridManager.Instance.GetMoveRangeGrids(current, actionPoint);
        List<GridTile> results = new List<GridTile>();
        sw.Stop();

        //Debug.Log("Elapsed SearchByGridPosition= " + sw.Elapsed);

        sw.Start();
        //Add self first
        results.Add(current);

        foreach (GridTile tile in tilesInRange)
        {
            var path = AStar.Search(current, tile);
            if (path == null) continue;

            int totoalCost = 0;

            foreach (GridTile t in path)
            {
                totoalCost += t.Cost();
                if (totoalCost <= actionPoint)
                {
                    if (!results.Contains(t))
                        results.Add(t);
                }
                else break;
            }
        }

        sw.Stop();
        //Debug.Log("Elapsed FilterByCost= " + sw.Elapsed);

        return results;
    }

    public static List<GridTile> SearchByGridPosition(GridTile start, int reach, bool ignoresHeight = false, bool includeStartingTile = false) {
        List<GridTile> closed = new List<GridTile>();
        List<GridTile> tilesInRange = new List<GridTile>();
        SimplePriorityQueue<GridTile> frontier = new SimplePriorityQueue<GridTile>();
        frontier.Enqueue(start, 0);
        while (frontier.Count > 0) {
            GridTile current = frontier.Dequeue();
            if (!tilesInRange.Contains(current)) {
                tilesInRange.Add(current);
                closed.Add(current);
            }

            foreach (GridTile next in GridManager.Instance.Neighbors(current, ignoresHeight, defaultSixDirections)) {

                if (closed.Contains(next))
                    continue;

                float priority = Heuristic(next, start);
                if (priority <= reach) {
                    frontier.Enqueue(next, priority);
                }
            }
        }

        // remove the starting tile if required
        if (!includeStartingTile) {
            if (tilesInRange.Contains(start)) {
                tilesInRange.Remove(start);
            }
        }

        return tilesInRange;
    }

    public static List<GridTile> HexSearchByGridPosition(GridTile start, int reach, bool ignoresHeight = false, bool includeStartingTile = false) {
        List<GridTile> closed = new List<GridTile>();
        List<GridTile> tilesInRange = new List<GridTile>();
        SimplePriorityQueue<GridTile> frontier = new SimplePriorityQueue<GridTile>();
        frontier.Enqueue(start, 0);

        GridTile current = start;

        while (frontier.Count > 0) {
            current = frontier.Dequeue();
            if (!tilesInRange.Contains(current)) {
                tilesInRange.Add(current);
                closed.Add(current);
            }

            foreach (GridTile next in GridManager.Instance.Neighbors(current, ignoresHeight, defaultSixDirections)) {

                if (closed.Contains(next))
                    continue;

                float priority = HexDistance(next, start);
                if (priority <= reach) {
                    frontier.Enqueue(next, priority);
                }
            }
        }

        // remove the starting tile if required
        if (!includeStartingTile) {
            if (tilesInRange.Contains(start)) {
                tilesInRange.Remove(start);
            }
        }

        return tilesInRange;
    }

    public static float Heuristic(GridTile a, GridTile b) {
        return Mathf.Abs(a.m_GridPosition.x - b.m_GridPosition.x) + Mathf.Abs(a.m_GridPosition.y - b.m_GridPosition.y);
    }

    public static float HexCubeDistance(Vector3Int a, Vector3Int b) {
        return Mathf.Max(Mathf.Abs(a.x - b.x), Mathf.Abs(a.y - b.y), Mathf.Abs(a.z - b.z));
    }

    public static Vector3Int OddRToCubeCoordinattes(Vector2Int gridPosition) {
        var x = gridPosition.x - (gridPosition.y - (gridPosition.y&1)) / 2;
        var z = gridPosition.y;
        var y = -x-z;
        return new Vector3Int(x, y, z);
    }

    public static float HexDistance(GridTile a, GridTile b) {
        var aCube = OddRToCubeCoordinattes(a.m_GridPosition);
        var bCube = OddRToCubeCoordinattes(b.m_GridPosition);
        return HexCubeDistance(aCube, bCube);
    }

    public static List<Vector2Int> defaultSixDirections = new List<Vector2Int>() {
        new Vector2Int( -1, 0 ), // left
        new Vector2Int( 0, 1 ),  // top
        new Vector2Int( 1, 0 ),  // right
        new Vector2Int( 0, -1 ), // bottom
        // Diagonals
        new Vector2Int( 1, 1 ),
        new Vector2Int( 1, -1 ),
        new Vector2Int( -1, 1 ),
        new Vector2Int( -1, -1 ),

    };

    public static List<GridTile> LinearRange(GridTile source, int length)
    {
        List<GridTile> attackRanges = new List<GridTile>();
        int start_x = source.m_GridPosition.x;
        int start_y = source.m_GridPosition.y;

        for (int i = -length; i <= length; i++)
        {
            if (i == 0) continue;

            Vector2Int pos_x = new Vector2Int(start_x + i, start_y);
            Vector2Int pos_y = new Vector2Int(start_x, start_y + i);

            if (GridManager.ExistsTileAtPosition(pos_x))
            {
                GridTile tile = GridManager.GetGridTileAtPosition(pos_x);
                attackRanges.Add(tile);
            }

            if (GridManager.ExistsTileAtPosition(pos_y))
            {
                attackRanges.Add(GridManager.GetGridTileAtPosition(pos_y));
            }
        }

        return attackRanges;
    }
}
