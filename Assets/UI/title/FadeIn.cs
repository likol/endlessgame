﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class FadeIn : MonoBehaviour
{
    public float m_Speed;
    public float m_waitForTime;
    private CanvasGroup m_selfCG;

    // Start is called before the first frame update
    void Awake()
    {
        m_selfCG = GetComponent<CanvasGroup>();
        m_selfCG.alpha = 0;

        StartCoroutine(WaitTimeAndFadeIn());
    }

    IEnumerator WaitTimeAndFadeIn()
    {
        yield return new WaitForSeconds(m_waitForTime);

        while (true)
        {
            if (m_selfCG.alpha > 0.999) break;
            m_selfCG.alpha = Mathf.Lerp(m_selfCG.alpha, 1, m_Speed);
            yield return new WaitForEndOfFrame();
        }
    }
}
