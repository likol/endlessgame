﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFog : MonoBehaviour
{
    public float m_Speed = 2f;
    public float m_length = -1f;
    public float m_offset = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = new Vector3(transform.position.x, Mathf.PingPong(m_Speed * Time.time, m_length), transform.position.z);

        transform.position = pos;
    }
}
