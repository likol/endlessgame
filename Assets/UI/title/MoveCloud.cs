﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCloud : MonoBehaviour
{
    public float m_Speed;
    public float m_angle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        float angle = Mathf.Sin(Time.time * m_Speed) * m_angle;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
